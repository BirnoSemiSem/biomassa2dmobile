using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using Mirror;
using UnityEngine;

public enum EntityIndex
{
    None,
    Player,
    Bot,
    Prop
}

public abstract class ObjEnt
{
    public int id;
    public GameObject Obj;

    public ObjEnt(int id, GameObject Obj)
    {
        this.id = id;
        this.Obj = Obj;
    }

    public ObjEnt()
    {

    }
}

public class Entity
{
    public class PlayerEnt : ObjEnt
    {
        public Player PlayerProperties;

        public PlayerEnt(int id, GameObject Obj, Player player) : base(id, Obj)
        {
            this.PlayerProperties = player;
        }

        public PlayerEnt()
        {
            
        }
    }

    public class NpcEnt : ObjEnt
    {
        public BotControl bot;

        public NpcEnt(int id, GameObject Obj, BotControl bot) : base(id, Obj)
        {
            this.bot = bot;
        }

        public NpcEnt()
        {
            
        }
    }

    public class PropEnt : ObjEnt
    {
        public PropEnt(int id, GameObject Obj) 
        {

        }

        public PropEnt()
        {
            
        }
    }

    public EntityIndex Index;

    public PlayerEnt Player;

    public NpcEnt NPC;

    public PropEnt Prop;

    public Entity(int id, GameObject Obj, Player player)
    {
        this.Index = EntityIndex.Player;
        this.Player = new PlayerEnt(id, Obj, player);
        this.NPC = null;
        this.Prop = null;
    }

    public Entity(NpcEnt _botEnt)
    {
        this.Index = EntityIndex.Bot;
        this.Player = null;
        this.NPC = _botEnt;
        this.Prop = null;
    }

    public Entity(PropEnt _propEnt)
    {
        this.Index = EntityIndex.Prop;
        this.Player = null;
        this.NPC = null;
        this.Prop = _propEnt;
    }

    public Entity()
    {

    }
}

public static class PhysicalEntity
{
    public class EntityList
    {
        internal List<Entity.PlayerEnt> playerEnts = new List<Entity.PlayerEnt>();
        internal List<Entity.NpcEnt> botEnts = new List<Entity.NpcEnt>();
        internal List<Entity.PropEnt> propEnts = new List<Entity.PropEnt>();
    }

    public static EntityList EntityLists {get; } = new EntityList();

    public static void SetEntityInList(int id, GameObject ent, EntityIndex index)
    {
        if(ent == null)
        {
            Debug.LogError($"{ent} is null!");
            return;
        }

        switch(index)
        {
            case EntityIndex.Player:
            {
                if(!EntityLists.playerEnts.Exists(x => x.Obj == ent))
                    EntityLists.playerEnts.Add( new Entity.PlayerEnt(id, ent, ent.GetComponent<Player>()));
                else
                    Debug.LogWarning($"{ent} is already exists");

                break;
            }
            case EntityIndex.Bot:
            {
                if(!EntityLists.botEnts.Exists(x => x.Obj == ent))
                    EntityLists.botEnts.Add( new Entity.NpcEnt(id, ent, ent.GetComponent<BotControl>()));
                else
                    Debug.LogWarning($"{ent} is already exists");

                break;
            }
            case EntityIndex.Prop:
            {
                if(!EntityLists.propEnts.Exists(x => x.Obj == ent))
                    EntityLists.propEnts.Add( new Entity.PropEnt(id, ent));
                else
                    Debug.LogWarning($"{ent} is already exists");
                 
                break;
            }
        }
    }

    public static void RemoveEntityInList(int id, GameObject ent, EntityIndex index)
    {
        if(ent == null)
        {
            Debug.LogError($"{ent} is null!");
            return;
        }

        switch (index)
        {
            case EntityIndex.Player:
                RemoveEntityFromList(EntityLists.playerEnts, ent);
                break;
            case EntityIndex.Bot:
                RemoveEntityFromList(EntityLists.botEnts, ent);
                break;
            case EntityIndex.Prop:
                RemoveEntityFromList(EntityLists.propEnts, ent);
                break;
        }
    }

    private static void RemoveEntityFromList<T>(List<T> list, GameObject entity) where T : ObjEnt
    {
        ObjEnt itemToRemove = list.FirstOrDefault(item => item.Obj == entity);

        if (itemToRemove != null)
        {
            list.Remove((T)itemToRemove);
        }
        else
        {
            Debug.LogWarning($"{entity} not found in the list!");
        }
    }
}
