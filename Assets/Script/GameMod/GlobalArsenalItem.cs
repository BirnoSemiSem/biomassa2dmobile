using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class GlobalArsenalItem : NetworkBehaviour
{
    [SerializeField] private GameObject[] PrimaryWeaponParametrs;
    [SerializeField] private GameObject[] SecondWeaponParametrs;
    [SerializeField] private GameObject[] GrenadeParametrs;
    [SerializeField] private GameObject[] MineParametrs;
    [SerializeField] private GameObject[] OtherItemParametrs;
    [SerializeField] private GameObject[] KnifeParametrs;

    [SerializeField] private GameObject ZombieKnifeParametrs;

    void OnEnable()
    {
        GameControl.EventPreStartRound += InitializationPlayers;

        GameObjStateControl.EventSpawnPlayer_After += PlayerSpawn;
    }

    void OnDisable()
    {
        GameControl.EventPreStartRound -= InitializationPlayers;

        GameObjStateControl.EventSpawnPlayer_After -= PlayerSpawn;
    }



    void Start()
    {
        
    }

    void InitializationPlayers()
    {
        if (PhysicalEntity.EntityLists.playerEnts != null)
        {
            for (int i = 0; i < PhysicalEntity.EntityLists.playerEnts.Count; i++)
            {
                if (PhysicalEntity.EntityLists.playerEnts[i].PlayerProperties.PlayerTeam != global::Player.Team.Spectator)
                {
                    if (isServer)
                    {
                        PhysicalEntity.EntityLists.playerEnts[i].PlayerProperties.RandomWeaponPrimary = UnityEngine.Random.Range(0, PrimaryWeaponParametrs.Length);
                        PhysicalEntity.EntityLists.playerEnts[i].PlayerProperties.RandomWeaponSecond = UnityEngine.Random.Range(0, SecondWeaponParametrs.Length);
                        // GameControl.Players[i].GetComponent<Player>().RandomWeaponGrenade = UnityEngine.Random.Range(0, GrenadeParametrs.Length);
                    }
                }
            }
        }
    }

    public void PlayerSpawn(Entity.PlayerEnt Player, StatusFunc status)
    {
        Player _player = Player.PlayerProperties;
        
        if (_player.PlayerTeam == global::Player.Team.Human)
        {
            _player.Weapons = null;
            _player.Weapons = new Player.WeaponPlayer[3];

            SetWeapon(_player, 0, PrimaryWeaponParametrs[_player.RandomWeaponPrimary].GetComponent<WeaponSystem>());
            SetWeapon(_player, 1, SecondWeaponParametrs[_player.RandomWeaponSecond].GetComponent<WeaponSystem>());
            //SetWeapon(_player, 2, GrenadeWeapon[_player.RandomWeaponGrenade].GetComponent<WeaponSystem>());
        }

    }

    void SetWeapon(Player player, int slot, WeaponSystem weapon)
    {
        player.Weapons[slot].PositionSpr = weapon.PositionSpr;

        player.Weapons[slot].Weapon = weapon.Weapon;

        player.Weapons[slot].NameWeapon = weapon.NameWeapon;

        player.Weapons[slot].MaxClip = weapon.MaxClip;
        player.Weapons[slot].Clip = weapon.Clip;
        player.Weapons[slot].MaxAmmo = weapon.MaxAmmo;
        player.Weapons[slot].Ammo = weapon.Ammo;

        player.Weapons[slot].MaxRecoil = weapon.MaxRecoil;

        player.Weapons[slot].FireSpeed = weapon.FireSpeed;
        player.Weapons[slot].SpeedBullet = weapon.SpeedBullet;
        player.Weapons[slot].ReloadTime = weapon.ReloadTime;

        player.Weapons[slot].MinDmg = weapon.MinDmg;
        player.Weapons[slot].MaxDmg = weapon.MaxDmg;
        player.Weapons[slot].HSDmg = weapon.HSDmg;

        player.Weapons[slot].Knockback = weapon.Knockback;

        player.Weapons[slot].WeaponSpr = weapon.WeaponSpr;

        player.Weapons[slot].FireClip = weapon.FireClip;
        player.Weapons[slot].DrawClip = weapon.DrawClip;
        player.Weapons[slot].ReloadClips = weapon.ReloadClips;

        player.Weapons[slot].KnifeMissClips = weapon.KnifeMissClips;
        player.Weapons[slot].KnifeStrikeClips = weapon.KnifeStrikeClips;

        if (slot == player.ActiveWeapon)
        {
            player.FireClip = weapon.FireClip;
            player.DrawClip = weapon.DrawClip;
            player.ReloadClips = weapon.ReloadClips;

            player.KnifeMissClips = weapon.KnifeMissClips;
            player.KnifeStrikeClips = weapon.KnifeStrikeClips;

            player.WeaponTransform.localPosition = player.Weapons[slot].PositionSpr;
            player.WeaponSprite.sprite = player.Weapons[slot].WeaponSpr;

            if (player.PlayerTeam == Player.Team.Human)
            {
                Player.AnimationPlayer animationPlayer = (Player.AnimationPlayer)player.ActiveWeapon + 1;
                AnimationObj.AnimationPlayer(player, player.HumanClass.Name + "-" + animationPlayer.ToString());
            }
        }
    }
}
