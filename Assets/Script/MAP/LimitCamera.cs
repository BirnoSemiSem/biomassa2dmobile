using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitCamera : MonoBehaviour
{
    public static LimitCamera limitCamera;

    public Transform leftWall;
    public Transform rightWall;
    public Transform upWall;
    public Transform downWall;

    void Start()
    {
        if(limitCamera == null)
            limitCamera = this;
    }

}
