﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    public static MapController MapProperties = null;

    public string MapName;
    public int MapTimeLimitMinut = 30;

    [SerializeField] private Vector2 WordPosition;

    // Start is called before the first frame update
    void Start()
    {
        if (MapProperties == null)
            MapProperties = this;

        transform.position = WordPosition;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
