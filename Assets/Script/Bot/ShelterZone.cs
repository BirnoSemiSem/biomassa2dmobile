﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShelterZone : MonoBehaviour
{
    public Vector2 Zone = Vector2.zero;

    [SerializeField] private bool IsDebugMod = false;

    private void OnDrawGizmos()
    {
        if (!IsDebugMod)
            return;

        Gizmos.color = Color.red;

        Gizmos.DrawLine(new Vector3(transform.position.x - Zone.x, transform.position.y + Zone.y), new Vector3(transform.position.x + Zone.x, transform.position.y + Zone.y));
        Gizmos.DrawLine(new Vector3(transform.position.x - Zone.x, transform.position.y - Zone.y), new Vector3(transform.position.x + Zone.x, transform.position.y - Zone.y));

        Gizmos.DrawLine(new Vector3(transform.position.x - Zone.x, transform.position.y + Zone.y), new Vector3(transform.position.x - Zone.x, transform.position.y - Zone.y));
        Gizmos.DrawLine(new Vector3(transform.position.x + Zone.x, transform.position.y + Zone.y), new Vector3(transform.position.x + Zone.x, transform.position.y - Zone.y));
    }
}
