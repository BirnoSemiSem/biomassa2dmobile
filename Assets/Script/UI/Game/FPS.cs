﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FPS : MonoBehaviour
{
    // Start is called before the first frame update
    public Text FPSText;
    public Text PINGText;
    //public TMP_Text MenuPINGText;

    public float _hudRefreshRate = 1f;
    private float _timer;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.unscaledTime > _timer)
        {
            int fps = (int)(1f / Time.unscaledDeltaTime);
            FPSText.text = "FPS: " + fps;
            PINGText.text = "Ping: " +  Math.Round(NetworkTime.rtt * 1000) + "ms";
            //MenuPINGText.text = Math.Round(NetworkTime.rtt * 1000).ToString();
            _timer = Time.unscaledTime + _hudRefreshRate;
        }
    }
}
