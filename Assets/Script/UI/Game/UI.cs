﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class UI : NetworkBehaviour
{
    //GameControl
    [SerializeField] private TextMeshProUGUI RoundText;

    //Biohazard
    [SerializeField] private TextMeshProUGUI TextCountZombie;
    [SerializeField] private TextMeshProUGUI TextCountHuman;

    [SerializeField] private Image ResultRound;

    [SerializeField] private Sprite SprHumanWin;
    [SerializeField] private Sprite SprZombieWin;

    // Player
    [SerializeField] private GameObject Player;
    [SerializeField] private ControlController Control;

    [SerializeField] private Image HealthBar;
    [SerializeField] private Image ArmorBar;
    [SerializeField] private TextMeshProUGUI TextHealth;
    [SerializeField] private TextMeshProUGUI TextArmor;

    [SerializeField] private GameObject WeaponStats;
    [SerializeField] private GameObject ArmorStats;
    [SerializeField] private TextMeshProUGUI TextNameWeapon;
    [SerializeField] private TextMeshProUGUI TextClipWeapon;
    [SerializeField] private TextMeshProUGUI TextAmmoWeapon;

    [SerializeField] private Text TextMoney;

    [SerializeField] private ButtonUI[] buttonUIs;
    [SerializeField] private Slider PrograssBarReload;
    [SerializeField] private TextMeshProUGUI TextProgressBar;

    [SerializeField] private GameObject PlayerStats;

    [SerializeField] private Joystic JoyStick;

    [SerializeField] private GameObject DeadScreen;
    [SerializeField] private GameObject ButtonsAlive;
    [SerializeField] private GameObject ButtonsDied;
    [SerializeField] private GameObject ButtonsChat;
    [SerializeField] private GameObject ButtonsMenu;

    private Coroutine CorPrograssFade;

    void Start()
    {
        NewRound();
    }

    void OnEnable()
    {
        GameControl.EventPreStartRound += NewRound;

        GameObjStateControl.EventSpawnPlayer_After += PlayerSpawn;
        GameObjStateControl.EventDeathPlayer_After += DeathPlayer;

        Biohazard.EventHumanWin += HumanWin;
        Biohazard.EventZombieWin += ZombieWin;
        Biohazard.EventInfectionPlayer += PlayerInfection;

        if(Player)
        {
            Control.PlayerControlLocal.weaponControl.EventPlayerStartReloading += PlayerStartReloading;
            Control.PlayerControlLocal.weaponControl.EventPlayerEndReloading += PlayerEndReloading;

            Control.PlayerControlLocal.weaponControl.EventPlayerStartDraw += PlayerStartDraw;
            Control.PlayerControlLocal.weaponControl.EventPlayerEndDraw += PlayerEndDraw;
        }
    }

    void OnDisable()
    {
        GameControl.EventPreStartRound -= NewRound;

        GameObjStateControl.EventSpawnPlayer_After -= PlayerSpawn;
        GameObjStateControl.EventDeathPlayer_After -= DeathPlayer;


        Biohazard.EventHumanWin -= HumanWin;
        Biohazard.EventZombieWin -= ZombieWin;
        Biohazard.EventInfectionPlayer -= PlayerInfection;

        if(Player)
        {
            Control.PlayerControlLocal.weaponControl.EventPlayerStartReloading -= PlayerStartReloading;
            Control.PlayerControlLocal.weaponControl.EventPlayerEndReloading += PlayerEndReloading;

            Control.PlayerControlLocal.weaponControl.EventPlayerStartDraw += PlayerStartDraw;
            Control.PlayerControlLocal.weaponControl.EventPlayerEndDraw += PlayerEndDraw;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!Player)
        {
            if (PhysicalEntity.EntityLists.playerEnts != null)
            {
                for (int i = 0; i < PhysicalEntity.EntityLists.playerEnts.Count; i++)
                {
                    if (PhysicalEntity.EntityLists.playerEnts[i].Obj && PhysicalEntity.EntityLists.playerEnts[i].PlayerProperties.isLocalPlayer)
                    {
                        Player = PhysicalEntity.EntityLists.playerEnts[i].Obj;
                        Control.PlayerControlLocal = Player.GetComponent<Player>();
                        Control.controllerPlayer = Player.GetComponent<ControllerPlayer>();

                        Control.PlayerControlLocal.weaponControl.EventPlayerStartReloading += PlayerStartReloading;
                        Control.PlayerControlLocal.weaponControl.EventPlayerEndReloading += PlayerEndReloading;

                        Control.PlayerControlLocal.weaponControl.EventPlayerStartDraw += PlayerStartDraw;
                        Control.PlayerControlLocal.weaponControl.EventPlayerEndDraw += PlayerEndDraw;

                        if (Control.Control == ControlController.Controller.Android)
                        {
                            for (int id = 0; id < buttonUIs.Length; id++)
                            {
                                buttonUIs[id].Player = Player;
                                buttonUIs[id].WeaponControl = Player.GetComponent<WeaponPlayerControl>();
                            }
                        }
                    }
                }
            }
        }

        if (Player)
        {
            if((!ButtonsChat.activeInHierarchy || !ButtonsMenu.activeInHierarchy) && Control.Control == ControlController.Controller.Android)
            {
                ButtonsChat.SetActive(true);
                ButtonsMenu.SetActive(true);
            }
            else if (Control.PlayerControlLocal && Control.PlayerControlLocal.IsAlive)
            {
                if (!ButtonsAlive.activeInHierarchy && Control.Control == ControlController.Controller.Android)
                {
                    JoyStick.Reset();
                    ButtonsAlive.SetActive(true);

                    ButtonsDied.SetActive(false);
                }

                if (DeadScreen.activeInHierarchy)
                    DeadScreen.SetActive(false);

                if (!PlayerStats.activeInHierarchy)
                    PlayerStats.SetActive(true);

                Hud(Control.PlayerControlLocal);
            }
            else if(Control.PlayerControlLocal && !Control.PlayerControlLocal.IsAlive && Control.PlayerControlSpec == null)
            {
                Hud(Control.PlayerControlLocal);

                if (Control.PlayerControlLocal.PlayerTeam != global::Player.Team.Spectator)
                    Invoke(nameof(SpectrMode), 3f);
                else
                    SpectrMode();

                if(!DeadScreen.activeInHierarchy)
                    DeadScreen.SetActive(true);

                if (!ButtonsDied.activeInHierarchy && Control.Control == ControlController.Controller.Android)
                {
                    ButtonsDied.SetActive(true);

                    JoyStick.Reset();
                    ButtonsAlive.SetActive(false);
                }
            }
            else if (Control.PlayerControlSpec && Control.PlayerControlSpec.IsAlive)
            {
                if (!ButtonsDied.activeInHierarchy && Control.Control == ControlController.Controller.Android)
                {
                    ButtonsDied.SetActive(true);

                    JoyStick.Reset();
                    ButtonsAlive.SetActive(false);
                }

                if(Control.PlayerControlSpec.PlayerTeam == global::Player.Team.Human && !WeaponStats.activeInHierarchy)
                {
                    WeaponStats.SetActive(true);
                    ArmorStats.SetActive(true);
                }
                else if(Control.PlayerControlSpec.PlayerTeam == global::Player.Team.Zombie && WeaponStats.activeInHierarchy)
                {
                    WeaponStats.SetActive(false);
                    ArmorStats.SetActive(false);
                }

                if (!PlayerStats.activeInHierarchy)
                    PlayerStats.SetActive(true);

                Hud(Control.PlayerControlSpec);
            }
            else if(Control.PlayerControlSpec && !Control.PlayerControlSpec.IsAlive)
            {
                Hud(Control.PlayerControlSpec);

                Invoke(nameof(SpectrMode), 3f);

                if (!DeadScreen.activeInHierarchy)
                    DeadScreen.SetActive(true);

                if (!ButtonsDied.activeInHierarchy && Control.Control == ControlController.Controller.Android)
                {
                    ButtonsDied.SetActive(true);

                    JoyStick.Reset();
                    ButtonsAlive.SetActive(false);
                }
            }
            else
            {
                PlayerStats.SetActive(false);

                if (Control.Control == ControlController.Controller.Android)
                {
                    ButtonsDied.SetActive(false);

                    JoyStick.Reset();
                    ButtonsAlive.SetActive(false);
                }
            }
        }

        if(Control.Control != ControlController.Controller.Android)
        {
            if(ButtonsDied.activeInHierarchy)
                ButtonsDied.SetActive(false);

            if (ButtonsAlive.activeInHierarchy)
            {
                JoyStick.Reset();
                ButtonsAlive.SetActive(false);
            }
        }

        TextCountZombie.text = Biohazard.GetZombieCount.ToString();
        TextCountHuman.text = Biohazard.GetHumanCount.ToString();
        RoundText.text = $"{(GameControl.GetTextTimeMinut > 9 ? "" : "0")}{GameControl.GetTextTimeMinut}:{(GameControl.GetTextTimeSecond > 9 ? "" : "0")}{GameControl.GetTextTimeSecond}";
    }

    public void NewRound()
    {
        if (ResultRound.sprite != null)
        {
            ResultRound.sprite = null;
            ResultRound.color = new Color(255f, 255f, 255f, 0f);
        }
    }

    void Hud(Player PlayerControl)
    {
        switch (PlayerControl.PlayerTeam)
        {
            case global::Player.Team.Zombie:
            {
                TextHealth.text = $"{(PlayerControl.Health > 0 ? PlayerControl.Health : 0)}";

                if (PlayerControl.ZombieClass.Health > 0f)
                    HealthBar.fillAmount = PlayerControl.Health > 0f ? (float)PlayerControl.Health / (float)PlayerControl.ZombieClass.Health : 0f;
                else
                    HealthBar.fillAmount = (float)PlayerControl.Health / 100f;
                ArmorBar.fillAmount = 0f;
                break;
            }
            case global::Player.Team.Human:
            {
                TextHealth.text = $"{(PlayerControl.Health > 0 ? PlayerControl.Health : 0)}";
                TextArmor.text = $"{(PlayerControl.Armor > 0 ? PlayerControl.Armor : 0)}";

                if (PlayerControl.HumanClass.Health > 0f)
                    HealthBar.fillAmount = PlayerControl.Health > 0f ? (float)(PlayerControl.Health / (float)PlayerControl.HumanClass.Health) : 0f;
                else
                    HealthBar.fillAmount = (float)PlayerControl.Health / 100f;
                if (PlayerControl.HumanClass.Armor > 0f)
                    ArmorBar.fillAmount = PlayerControl.Armor > 0f ? (float)(PlayerControl.Armor / (float)PlayerControl.HumanClass.Armor) : 0f;
                else
                    ArmorBar.fillAmount = (float)PlayerControl.Armor / 100f;
                break;
            }
        }

        if (PlayerControl.IsAlive && PlayerControl.PlayerTeam == global::Player.Team.Human)
        {
            TextNameWeapon.text = PlayerControl.Weapons[PlayerControl.ActiveWeapon].NameWeapon;
            TextClipWeapon.text = PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip.ToString();
            TextAmmoWeapon.text = PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo.ToString();
        }
    }

    void PlayerStartReloading(Player player, int id)
    {
        if(!player.isLocalPlayer)
            return;

        if (PrograssBarReload)
        {
            PrograssBarReload.gameObject.SetActive(true);
            PrograssBarReload.value = 0;
            TextProgressBar.text = "Перезарядка...";

            if (CorPrograssFade != null)
                StopCoroutine(CorPrograssFade);
            CorPrograssFade = StartCoroutine(PrograssBarFade(PrograssBarReload, player.Weapons[player.ActiveWeapon].ReloadTime));
        }
    }

    void PlayerEndReloading(Player player, int id)
    {
        if(!player.isLocalPlayer)
            return;

        if (PrograssBarReload)
            PrograssBarReload.gameObject.SetActive(false);
    }

    void PlayerStartDraw(Player player, int id)
    {
        if(!player.isLocalPlayer)
            return;

        if (PrograssBarReload)
        {
            PrograssBarReload.gameObject.SetActive(true);
            PrograssBarReload.value = 0;
            TextProgressBar.text = "Смена оружия...";

            if(CorPrograssFade != null)
                StopCoroutine(CorPrograssFade);

            CorPrograssFade = StartCoroutine(PrograssBarFade(PrograssBarReload, 1f));
        }
    }

    void PlayerEndDraw(Player player, int id)
    {
        if(!player.isLocalPlayer)
            return;

        if (PrograssBarReload)
            PrograssBarReload.gameObject.SetActive(false);
    }

    void SpectrMode()
    {
        CancelInvoke(nameof(SpectrMode));
        Control.SpectrMode();
    }

    void ResetPlayerWeaponProperties(Entity.PlayerEnt Player, StatusFunc status = null)
    {
        if (Player.PlayerProperties.isLocalPlayer)
        {
            if (PrograssBarReload)
            {
                PrograssBarReload.gameObject.SetActive(false);
                PrograssBarReload.value = 0;
                TextProgressBar.text = "";

                if (CorPrograssFade != null)
                    StopCoroutine(CorPrograssFade);
            }
        }
    }

    public void PlayerSpawn(Entity.PlayerEnt Player, StatusFunc status)
    {
        if (!Control.PlayerControlLocal)
            return;

        if (Player.id == Control.PlayerControlLocal.GetPlayerID)
        {
            if (Control.PlayerControlLocal.IsAlive && Control.PlayerControlLocal.PlayerTeam == global::Player.Team.Human)
            {
                WeaponStats.SetActive(true);
                ArmorStats.SetActive(true);
            }

            DeadScreen.SetActive(false);

            Control.CameraLocalPlayer.ChangePlayerTarget(Player.Obj);

            CancelInvoke(nameof(SpectrMode));
            Control.IndexSpec = -1;
            Control.PlayerControlSpec = null;
        }

        ResetPlayerWeaponProperties(Player);
    }

    public void DeathPlayer(Entity.PlayerEnt Player, StatusFunc status)
    {
        if (!Control.PlayerControlLocal)
            return;

        ResetPlayerWeaponProperties(Player);
    }

    void ZombieWin()
    {
        if (ResultRound != null)
        {
            ResultRound.sprite = SprZombieWin;
            ResultRound.color = new Color(255f, 255f, 255f, 255f);
        }
    }

    void HumanWin()
    {
        if (ResultRound != null)
        {
            ResultRound.sprite = SprHumanWin;
            ResultRound.color = new Color(255f, 255f, 255f, 255f);
        }
    }

    void PlayerInfection(Player Player, int id)
    {
        if (!Control.PlayerControlLocal)
            return;

        if (id == Control.PlayerControlLocal.GetPlayerID)
        {
            if (Control.PlayerControlLocal.IsAlive)
            {
                WeaponStats.SetActive(false);
                ArmorStats.SetActive(false);
            }
        }

        ResetPlayerWeaponProperties(new Entity(id, Player.gameObject, Player).Player);
    }

    public static IEnumerator PrograssBarFade(Slider prograss, float duration)
    {
        float currentTime = 0;
        float start = prograss.value;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            prograss.value = Mathf.Lerp(start, 1f, currentTime / duration);
            yield return null;
        }
        yield break;
    }
}
