﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Joystic : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    // Start is called before the first frame update

    public GameObject marker;
    public int MaxRadios = 100;
    Vector2 TargetVector;
    Vector2 PoinerPosition;

    public ControllerPlayer controllerPlayer;

    private bool IsMoveZone = false;

    private bool IsPointerDown = false;

    void Start()
    {
        marker.transform.position = transform.position;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        IsPointerDown = true;
        PoinerPosition = eventData.position;
    }

    public void OnDrag(PointerEventData eventData)
    {
        IsPointerDown = true;
        PoinerPosition = eventData.position;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        IsPointerDown = false;
        PoinerPosition = Vector2.zero;

        marker.transform.position = transform.position;

        if(controllerPlayer)
            controllerPlayer.ChangeTargetMove(new Vector2(0, 0));

        IsMoveZone = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!controllerPlayer)
        {
            if (PhysicalEntity.EntityLists.playerEnts != null)
            {
                for (int i = 0; i < PhysicalEntity.EntityLists.playerEnts.Count; i++)
                    if (PhysicalEntity.EntityLists.playerEnts[i].PlayerProperties.isLocalPlayer)
                        controllerPlayer = PhysicalEntity.EntityLists.playerEnts[i].Obj.GetComponent<ControllerPlayer>();
            }
        }

        if (IsPointerDown && controllerPlayer)
        {
            TargetVector = PoinerPosition - (Vector2)transform.position;

            if (TargetVector.magnitude < MaxRadios || IsMoveZone)
            {
                if (TargetVector.magnitude < MaxRadios * 5)
                {
                    if (TargetVector.magnitude < MaxRadios)
                        marker.transform.position = PoinerPosition;
                    else
                        marker.transform.position = transform.position + ((Vector3)PoinerPosition - transform.position).normalized * MaxRadios;
                    controllerPlayer.ChangeTargetMove(TargetVector);

                    IsMoveZone = true;
                }
                else
                {
                    marker.transform.position = transform.position;
                    controllerPlayer.ChangeTargetMove(new Vector2(0, 0));
                    IsMoveZone = false;

                    IsPointerDown = false;
                    PoinerPosition = Vector2.zero;
                }
            }
            else
            {
                marker.transform.position = transform.position;
                controllerPlayer.ChangeTargetMove(new Vector2(0, 0));
                IsMoveZone = false;

                IsPointerDown = false;
                PoinerPosition = Vector2.zero;
            }
        }
    }

    public void Reset()
    {
        IsPointerDown = false;
        PoinerPosition = Vector2.zero;

        marker.transform.position = transform.position;

        if (controllerPlayer)
            controllerPlayer.ChangeTargetMove(new Vector2(0, 0));

        IsMoveZone = false;
    }
}
