﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ErrorMenu : MonoBehaviour
{
    [SerializeField] private GameObject ErrorWindow;
    [SerializeField] private TextMeshProUGUI ErrorText;

    public Button BtnContinuous;
    public TextMeshProUGUI BtnContinuousText;

    public Button BtnExit;
    public TextMeshProUGUI BtnExitText;

    public void ErrorWindowUp(string status)
    {
        ErrorWindow.SetActive(true);

        ErrorText.text = status;
    }

    public void ErrorWindowClear()
    {
        ErrorWindow.SetActive(false);
        ErrorText.text = null;

        BtnContinuous.onClick.RemoveAllListeners();
        BtnExit.onClick.RemoveAllListeners();
    }
}
