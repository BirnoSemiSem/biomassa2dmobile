﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LoadingMenu : MonoBehaviour
{
    [SerializeField] private GameObject LoadingWindow;
    [SerializeField] private TextMeshProUGUI TextLoading;

    private Coroutine CorLoading;

    public void LoadingWindowUp(string status)
    {
        LoadingWindow.SetActive(true);

        if(CorLoading != null)
            StopCoroutine(CorLoading);

        TextLoading.text = status;
        CorLoading = StartCoroutine(LoadingStatus());
    }

    public void LoadingWindowClear()
    {
        LoadingWindow.SetActive(false);
        TextLoading.text = null;

        if (CorLoading != null)
            StopCoroutine(CorLoading);
    }

    IEnumerator LoadingStatus()
    {
        string buffer = TextLoading.text;
        int CountPoint = 0;

        while (LoadingWindow.activeSelf)
        {
            yield return new WaitForSeconds(0.5f);
            CountPoint++;

            if (CountPoint > 3)
            {
                CountPoint = 1;
                TextLoading.text = buffer;
            }

            TextLoading.text += ".";
        }
    }
}
