﻿using LightReflectiveMirror;
using Mirror;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuControl : MonoBehaviour
{
    [SerializeField] private GameObject MirrorNet;
    private Coroutine CorNetwork;

    [SerializeField] private AudioSource AudioMenu;

    [SerializeField] private AuthManager AuthManager;
    public bool IsFirstLogin = false;

    //Login
    [SerializeField] private GameObject LoginWindow;
    [SerializeField] private GameObject RegisterWindow;

    //Menu
    [SerializeField] private GameObject MainMenu;

    [SerializeField] private GameObject ServerBrowser;
    [SerializeField] private GameObject HostGame;

    [SerializeField] private GameObject SettingsMenu;
    [SerializeField] private GameObject SettingsAdvenceMenu;
    [SerializeField] private GameObject SettingsKeyControlMenu;

    [SerializeField] private GameObject Dialog;

    [SerializeField] private GameObject WelcomToTheGame;

    //Elements Window
    [SerializeField] private List<GameObject> Windows;

    //Window
    public LoadingMenu LoadingWindow;
    public ErrorMenu ErrorWindow;
    public DialogMenu DialogWindow;

    void Start()
    {
        if (SpecificalOptions.specificalOptions != null)
            Destroy(MirrorNet);

        CorNetwork = StartCoroutine(ConnectingToLRM());
    }

    void LoginMenu()
    {
        LoadingWindow.LoadingWindowClear();

        LoginWindow.SetActive(true);
    }

    public void GameStartConnect()
    {
        StartCoroutine(ConnectingToServer());
        MainMenu.SetActive(false);
    }

    public void GameStartHost()
    {
        StartCoroutine(HostToServer());
        MainMenu.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (MainMenu.activeInHierarchy)
                DialogWindow.EscToGameExit();
            else
                DelWindows(Windows.Count-1);
        }

        if(MainMenu.activeInHierarchy && !WelcomToTheGame.activeInHierarchy && IsFirstLogin)
        {
            AddWindows(WelcomToTheGame, false);

            IsFirstLogin = false;
        }

        if (!SpecificalOptions.specificalOptions._LightTransport.Available() && CorNetwork == null)
            CorNetwork = StartCoroutine(ConnectingToLRM());

        if (AudioMenu && AudioMenu.volume != SpecificalOptions.specificalOptions.AudioMusicVolume)
            AudioMenu.volume = SpecificalOptions.specificalOptions.AudioMusicVolume;
    }

    public void Back()
    {
        DelWindows(Windows.Count-1);
    }

    public void ClickWindowServerBrowser()
    {
        AddWindows(ServerBrowser, true);
    }

    public void ClickWindowHostGame()
    {
        AddWindows(HostGame, true);
    }

    public void ClickWindowSettingsMenu()
    {
        AddWindows(SettingsMenu, true);
    }

    public void ClickWindowSettingsAdvanceMenu()
    {
        AddWindows(SettingsAdvenceMenu, true);
    }

    public void ClickWindowSettingsKeyControlMenu()
    {
        AddWindows(SettingsKeyControlMenu, true);
    }

    public void ClickWindowWelcom()
    {
        AddWindows(WelcomToTheGame, false);
    }

    public void ClickOpenTelegram()
    {
        Application.OpenURL("https://t.me/biomassa2d");
    }

    public void ClickOpenVK()
    {
        Application.OpenURL("https://vk.com/biomassa2d");
    }

    public void ClickOpenDS()
    {
        Application.OpenURL("https://discord.gg/v2HgTAPaxF");
    }

    void AddWindows(GameObject Window, bool IsCloseWindows)
    {
        if (MainMenu.activeInHierarchy && IsCloseWindows)
            MainMenu.SetActive(false);

        if(IsCloseWindows)
            for (int i = 0; i < Windows.Count; i++)
                if (Windows[i].activeInHierarchy)
                    Windows[i].SetActive(false);

        Windows.Add(Window);
        Window.SetActive(true);
    }

    void DelWindows(int index)
    {
        if(Windows.Count > 0)
        {
            if(Windows[index] != null)
            {
                Windows[index].SetActive(false);
                Windows.RemoveAt(index);
            }

            if (Windows.Count <= 0)
                MainMenu.SetActive(true);
            else
                Windows[Windows.Count - 1].SetActive(true);
        }
    }

    IEnumerator ConnectingToServer()
    {
        LoadingWindow.LoadingWindowUp("Подключение");

        yield return new WaitForSeconds(1f);

        if (!SpecificalOptions.specificalOptions._LightTransport.Available())
        {
            ErrorWindow.ErrorWindowUp("Причина: отсуствует подключение к мосту");

            ErrorWindow.BtnContinuous.onClick.AddListener(ClickReConnectRelay);
            ErrorWindow.BtnContinuousText.text = "Повторить";

            ErrorWindow.BtnExit.onClick.AddListener(ClickExitGame);
            ErrorWindow.BtnExitText.text = "Выход";

            yield break;
        }

        LoadingWindow.LoadingWindowUp("Загрузка");
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(SpecificalOptions.specificalOptions.Game);
        SpecificalOptions.specificalOptions.IsChangeScene = true;

        asyncOperation.allowSceneActivation = false;

        while (!asyncOperation.isDone)
        {
            if (asyncOperation.progress >= .9f)
                break;
            else
                yield return null;
        }

        NetworkManager.singleton.StartClient();
        LoadingWindow.LoadingWindowUp("Подключение");

        int CountTimeOut = 0;

        while (!NetworkClient.isConnecting && CountTimeOut < 5)
        {
            yield return new WaitForSeconds(2f);
            CountTimeOut++;
        }

        if (CountTimeOut >= 5)
        {
            ErrorWindow.ErrorWindowUp("Причина: невозможно подключиться к серверу");

            ErrorWindow.BtnContinuous.onClick.AddListener(ClickReConnectServer);
            ErrorWindow.BtnContinuousText.text = "Повторить";

            ErrorWindow.BtnExit.onClick.AddListener(ClickResetWindows);
            ErrorWindow.BtnExitText.text = "Назад";
            yield break;
        }

        SpecificalOptions.specificalOptions.IsOnlyClient = true;

        asyncOperation.allowSceneActivation = true;

        yield return null;
    }

    IEnumerator HostToServer()
    {
        LoadingWindow.LoadingWindowUp("Подключение");

        yield return new WaitForSeconds(1f);

        if (!SpecificalOptions.specificalOptions._LightTransport.Available())
        {
            ErrorWindow.ErrorWindowUp("Причина: отсуствует подключение к мосту");

            ErrorWindow.BtnContinuous.onClick.AddListener(ClickReConnectRelay);
            ErrorWindow.BtnContinuousText.text = "Повторить";

            ErrorWindow.BtnExit.onClick.AddListener(ClickExitGame);
            ErrorWindow.BtnExitText.text = "Выход";

            yield break;
        }

        LoadingWindow.LoadingWindowUp("Загрузка");
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(SpecificalOptions.specificalOptions.Game);
        SpecificalOptions.specificalOptions.IsChangeScene = true;

        asyncOperation.allowSceneActivation = false;

        while (!asyncOperation.isDone)
        {
            if (asyncOperation.progress >= .9f)
                break;
            else
                yield return null;
        }

        SpecificalOptions.specificalOptions.IsHost = true;

        asyncOperation.allowSceneActivation = true;

        yield return null;
    }

    IEnumerator ConnectingToLRM()
    {
        LoadingWindow.LoadingWindowUp("Подключение");

        yield return new WaitForSeconds(1f);

        if(SpecificalOptions.specificalOptions._LightTransport == null)
        {
            ErrorWindow.ErrorWindowUp("Причина: отсуствует клиентский сетевой мост");

            ErrorWindow.BtnContinuous.onClick.AddListener(ClickReConnectRelay);
            ErrorWindow.BtnContinuousText.text = "Повторить";

            ErrorWindow.BtnExit.onClick.AddListener(ClickExitGame);
            ErrorWindow.BtnExitText.text = "Выход";

            CorNetwork = null;
            yield break;
        }

        int CountTimeOut = 0;

        while(!SpecificalOptions.specificalOptions._LightTransport.Available() && CountTimeOut < 5)
        {
            yield return new WaitForSeconds(2f);
            CountTimeOut++;
        }

        if(!SpecificalOptions.specificalOptions._LightTransport.Available())
        {
            ErrorWindow.ErrorWindowUp("Причина: отсуствует подключение к мосту");

            ErrorWindow.BtnContinuous.onClick.AddListener(ClickReConnectRelay);
            ErrorWindow.BtnContinuousText.text = "Повторить";

            ErrorWindow.BtnExit.onClick.AddListener(ClickExitGame);
            ErrorWindow.BtnExitText.text = "Выход";

            CorNetwork = null;
            yield break;
        }

        if (SpecificalOptions.specificalOptions.PlayerInfo != null)
        {
            AuthManager.LogUpAutomatically(System.Text.Encoding.UTF8.GetString(SpecificalOptions.specificalOptions.PlayerInfo.Email), System.Text.Encoding.UTF8.GetString(SpecificalOptions.specificalOptions.PlayerInfo.Password));
        }
        else if (!IsInvoking(nameof(LoginMenu)))
        {
            Invoke(nameof(LoginMenu), 1f);
            IsFirstLogin = true;
        }

        CorNetwork = null;
    }

    public void ClickReConnectRelay()
    {
        if (SpecificalOptions.specificalOptions._LightTransport == null)
            SpecificalOptions.specificalOptions._LightTransport = (LightReflectiveMirrorTransport)Transport.activeTransport;

        SpecificalOptions.specificalOptions._LightTransport.ConnectToRelay();

        ErrorWindow.ErrorWindowClear();
        CorNetwork = StartCoroutine(ConnectingToLRM());
    }

    public void ClickReConnectServer()
    {
        if (SpecificalOptions.specificalOptions._LightTransport == null)
            SpecificalOptions.specificalOptions._LightTransport = (LightReflectiveMirrorTransport)Transport.activeTransport;

        NetworkManager.singleton.networkAddress = SpecificalOptions.specificalOptions.ServerOptions.ServerID;
        NetworkManager.singleton.StartClient();

        ErrorWindow.ErrorWindowClear();
        CorNetwork = StartCoroutine(ConnectingToServer());
    }

    public void ClickExitGame()
    {
        DialogWindow.EscToGameExit();
    }

    public void ClickResetWindows()
    {
        ErrorWindow.ErrorWindowClear();
        LoadingWindow.LoadingWindowClear();

        SpecificalOptions.specificalOptions.IsChangeScene = false;
    }
}
