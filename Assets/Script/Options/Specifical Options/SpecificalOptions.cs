﻿using LightReflectiveMirror;
using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpecificalOptions : MonoBehaviour
{
    public static SpecificalOptions specificalOptions;

    public ServerOptions ServerOptions;
    //Network
    public Net NetManager;
    public LightReflectiveMirrorTransport _LightTransport;

    //Scenes
    [Scene]
    public string MainMenu;
    [Scene]
    public string Game;

    public bool IsHost = false;
    public bool IsOnlyClient = false;

    public bool IsChangeScene = false;

    //Audio
    public float AudioEffectVolume = 0.0f;
    public float AudioMusicVolume = 0.0f;

    public PlayerInfo PlayerInfo;

    // Start is called before the first frame update
    void Awake()
    {
        specificalOptions = this;

        DontDestroyOnLoad(gameObject);

        if (_LightTransport == null)
            _LightTransport = (LightReflectiveMirrorTransport)Transport.activeTransport;

        if (NetManager == null)
            NetManager = _LightTransport.GetComponent<Net>();

        PlayerInfo = new PlayerInfo();
        PlayerInfo = DataSaver.loadData<PlayerInfo>("ProfilePlayer");

        if (PlayerPrefs.HasKey("AudioEffectVolume") && PlayerPrefs.HasKey("AudioMusicVolume"))
        {
            AudioEffectVolume = PlayerPrefs.GetFloat("AudioEffectVolume");
            AudioMusicVolume = PlayerPrefs.GetFloat("AudioMusicVolume");
        }
        else
            SetPlayerPrefsAudio();

        ServerOptions.ServerOptionsStart();
    }

    void OnApplicationQuit()
    {
        SpecificalOptions.specificalOptions._LightTransport.DisconnectFromRelay();
    }


    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void SetPlayerPrefsAudio()
    {
        AudioEffectVolume = 0.6f;
        AudioMusicVolume = 0.6f;

        PlayerPrefs.SetFloat("AudioEffectVolume", AudioEffectVolume);
        PlayerPrefs.SetFloat("AudioMusicVolume", AudioMusicVolume);

        PlayerPrefs.Save();
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (SceneManager.GetActiveScene().path == SpecificalOptions.specificalOptions.MainMenu)
        {
            SpecificalOptions.specificalOptions.IsHost = false;
            SpecificalOptions.specificalOptions.IsOnlyClient = false;

            SpecificalOptions.specificalOptions.ServerOptions.ServerID = null;
            SpecificalOptions.specificalOptions.ServerOptions.SelectMap = null;

            SpecificalOptions.specificalOptions.ServerOptions.SelectMode = null;
            SpecificalOptions.specificalOptions.ServerOptions.MapName = null;
            SpecificalOptions.specificalOptions.ServerOptions.CurrentMap = null;

            SpecificalOptions.specificalOptions.ServerOptions.TimeLeftMinut = 0;
            SpecificalOptions.specificalOptions.ServerOptions.TimeLeftSeconds = 0;

            SpecificalOptions.specificalOptions.ServerOptions.CountBots = 0;

            SpecificalOptions.specificalOptions.ServerOptions.IsChangeMap = false;
        }
        else
        {
            SpecificalOptions.specificalOptions.NetManager.GameScene();
            SpecificalOptions.specificalOptions.ServerOptions.LoadingMap();

            if (SpecificalOptions.specificalOptions.IsOnlyClient)
                SpecificalOptions.specificalOptions.NetManager.OnClientConnect();
            else if (SpecificalOptions.specificalOptions.IsHost)
                NetworkManager.singleton.StartHost();
        }

        SpecificalOptions.specificalOptions.IsChangeScene = false;
    }
}
