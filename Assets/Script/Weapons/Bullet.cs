﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Start is called before the first frame update
    public Rigidbody2D rb;

    [SerializeField] private GameObject _owner;
    [SerializeField] private Player _player;
    [SerializeField] private Player.WeaponPlayer _weaponPlayer;

    [SerializeField] private SpriteRenderer _spr;
    [SerializeField] private bool IsOptimization;

    void Start()
    {
        if(IsOptimization)
            _spr.color = new Color(255f, 255f, 255f, 0f);
    }

    void OnEnable()
    {
        GameControl.EventRoundEnd += InvisibleBullet;
    }

    void OnDisable()
    {
        GameControl.EventRoundEnd -= InvisibleBullet;
    }

    void OnBecameVisible()
    {
        if (gameObject && IsOptimization)
        {
            _spr.color = new Color(255f, 255f, 255f, 255f);
        }
    }

    void OnBecameInvisible()
    {
        if (gameObject && IsOptimization)
        {
            _spr.color = new Color(255f, 255f, 255f, 0f);
        }
    }

    public void Push(GameObject PlayerObject)
    {
        _owner = PlayerObject;
        _player = PlayerObject.GetComponent<Player>();
        _weaponPlayer = _player.Weapons[_player.ActiveWeapon];

        rb.velocity = transform.up * _weaponPlayer.SpeedBullet;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(!_owner) // Owner bullet obj is null or destroy.
        {
            InvisibleBullet();
            return;
        }

        if (_owner != col.gameObject && gameObject.CompareTag("Bullet") != col.CompareTag("Bullet"))
        {
            Player _victim = col.GetComponent<Player>();
            if (_victim)
            {
                if (_player.PlayerTeam == global::Player.Team.Human && _player.PlayerTeam != _victim.PlayerTeam)
                {
                    int Dmg = 0;
                    if (Random.Range(0, 10) > 7)
                        Dmg = _weaponPlayer.HSDmg;
                    else
                        Dmg = Random.Range(_weaponPlayer.MinDmg, _weaponPlayer.MaxDmg);

                    Entity EntVictim = new Entity(_victim.GetPlayerID, _victim.gameObject, _victim);
                    Entity EntAttacker = new Entity(_player.GetPlayerID, _player.gameObject, _player);

                    GameObjStateControl.gameObjStateControl.GOSC_TakeDamageObj(EntVictim, EntAttacker, Dmg);

                    InvisibleBullet();

                    //NetworkServer.Destroy(gameObject);

                    //if (isServer)
                    //    RpcDamage(col.gameObject, Player);
                    //else
                    //{
                    //    int Dmg = Random.Range(Weapon.MinDmg, Weapon.MaxDmg);
                    //    CmdDamage(col.gameObject, Player, Dmg);

                    //    Damage.RpcTakeDamege(col.gameObject, Player, Dmg);
                    //    KnockBack.TakeKnockBack(0.3f, 5f, col.gameObject, Player);
                    //}
                }
            }
            else if (!col.CompareTag("ClearGlass"))
            {
                InvisibleBullet();

                //NetworkServer.Destroy(gameObject);
            }
        }
    }

    void InvisibleBullet()
    {
        if (gameObject)
            gameObject.SetActive(false);

        _owner = null;
        _player = null;
        _weaponPlayer = new Player.WeaponPlayer();

        rb.velocity = Vector2.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeInHierarchy && (transform.position.x > 50 || transform.position.x < -50 || transform.position.y > 50 || transform.position.y < -50 || rb.velocity == Vector2.zero))
        {
            InvisibleBullet();

            //NetworkServer.Destroy(gameObject);
        }
    }
}
