﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitKnife : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Player _player;

    [SerializeField] private Collider2D ColliderZoneHitKnife;

    public bool IsHit = false;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!ColliderZoneHitKnife.enabled)
            IsHit = false;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        Player _victim = col.GetComponent<Player>();
        if (_player.isServer && _victim && _player.PlayerTeam == Player.Team.Zombie)
        {
            Entity EntVictim = new Entity(_victim.GetPlayerID, _victim.gameObject, _victim);
            Entity EntAttacker = new Entity(_player.GetPlayerID, _player.gameObject, _player);

            GameObjStateControl.gameObjStateControl.GOSC_TakeDamageObj(EntVictim, EntAttacker, Random.Range(_player.Weapons[0].MinDmg, _player.Weapons[0].MaxDmg));
        }
        IsHit = true;
    }
}
