using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TargetSearch : MonoBehaviour
{
    /*    static public Player PlayerSearch(Player _player, float RadiusPlayerDetect)
        {
            Player TempTargetEnemy = null;

            if (PhysicalEntity.EntityLists.playerEnts.Count > 0)
            {
                Player _playerTarget;
                for (int i = 0; i < PhysicalEntity.EntityLists.playerEnts.Count; i++)
                {
                    _playerTarget = PhysicalEntity.EntityLists.playerEnts[i].PlayerProperties;

                    if (_playerTarget != null && _playerTarget != _player && _playerTarget.IsAlive && _playerTarget.PlayerTeam != _player.PlayerTeam &&
                        Vector2.Distance(_player.transform.position, _playerTarget.transform.position) <= RadiusPlayerDetect)
                    {
                        Vector2 TargetLocation = _playerTarget.transform.position - _player.transform.position;

                        RaycastHit2D[] hitRay = Physics2D.RaycastAll(_player.transform.position, TargetLocation.normalized, RadiusPlayerDetect*2);
                        Player _HitTarget;

                        for(int id = 0; id < hitRay.Length; id++)
                        {
                            _HitTarget = hitRay[id].collider.GetComponent<Player>();

                            if(_HitTarget && _HitTarget.tag == "Player")
                            {
                                if(_playerTarget == _HitTarget)
                                {
                                    if (!TempTargetEnemy)
                                        TempTargetEnemy = _HitTarget;
                                    else if (Vector2.Distance(_player.transform.position, TempTargetEnemy.transform.position) > Vector2.Distance(_player.transform.position, _HitTarget.transform.position))
                                        TempTargetEnemy = _HitTarget;
                                }
                            }
                            else
                                break;
                        }
                    }
                }
            }

            if(TempTargetEnemy)
                return TempTargetEnemy;
            else 
                return null;
        }
    */

    public static Player PlayerSearch(Player _player, float detectionRadius)
    {
        Player closestEnemy = null;
        float shortestPathLength = float.MaxValue;

        ContactFilter2D contactFilter = new ContactFilter2D();
        contactFilter.useTriggers = false; // ������������ ��������, ���� �� �����
        contactFilter.SetLayerMask(LayerMask.GetMask("Obstacles", "Players"));

        // �������� ������� ������
        Vector2 playerPosition = _player.transform.position;

        // ������� ���� ������� � ������� �����������
        Collider2D[] hits = Physics2D.OverlapCircleAll(playerPosition, detectionRadius);

        foreach (var hit in hits)
        {
            // ���������, �������� �� ������ �������
            Player targetPlayer = hit.GetComponent<Player>();
            if (targetPlayer != null &&
                targetPlayer != _player &&
                targetPlayer.IsAlive &&
                targetPlayer.PlayerTeam != _player.PlayerTeam)
            {
                // ������������ ����������� �� ����
                Vector2 direction = (Vector2)targetPlayer.transform.position - playerPosition;

                // ��������� ��������� ���� ����� Raycast
                RaycastHit2D[] AllHit = new RaycastHit2D[5];
                int hitCount = Physics2D.Raycast(playerPosition, direction.normalized, contactFilter, AllHit, detectionRadius);

                for(int i = 0; i < hitCount; i++)
                {
                    Player hitPlayer = AllHit[i].collider.GetComponent<Player>();

                    if (hitPlayer == null)
                        break;

                    /*Debug.Log($"Own: {_player.name}, Col: {targetPlayer.name}, Ray: {hitPlayer.name}");*/

                    if (targetPlayer == hitPlayer && hitPlayer != _player) // ���������, ��� ��� �� ��� �����
                    {
                        // ������������ ���� � ������� NavMesh
                        NavMeshPath path = new NavMeshPath();
                        if (NavMesh.CalculatePath(_player.transform.position, targetPlayer.transform.position, NavMesh.AllAreas, path))
                        {
                            float pathLength = PathCalculate.CalculatePathDistance(path);
                            if (pathLength < shortestPathLength)
                            {
                                closestEnemy = targetPlayer;
                                shortestPathLength = pathLength;
                            }
                        }
                    }
                }
            }
        }

        return closestEnemy;
    }
}
