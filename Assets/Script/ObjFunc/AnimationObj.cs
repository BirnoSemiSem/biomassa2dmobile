using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationObj : MonoBehaviour
{
    static public void AnimationPlayer(Player _player, string NameAnim)
    {
        if(_player == null)
        {
            Debug.LogError($"{nameof(_player)} is null!");
            return;
        }

        if(String.IsNullOrEmpty(NameAnim))
        {
            Debug.LogError($"{nameof(NameAnim)} is null!");
            return;
        }

        _player.Animator.Play(NameAnim);
    }

    static public void AnimationPlayerFeet(Animator _feet, float _koffPlayerSpeed, Vector3 Move)
    {
        if (_feet == null)
        {
            Debug.LogError($"{nameof(_feet)} is null!");
            return;
        }

        //if (GameControl.GetIsFreeze)
        //    _feet.enabled = false;

        if (!_feet.enabled)
            return;

        if(Move != Vector3.zero)
            _feet.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, -(Mathf.Atan2(Move.x, Move.y) * Mathf.Rad2Deg)));

        _feet.speed = _koffPlayerSpeed;
    }
}
