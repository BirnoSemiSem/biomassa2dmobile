﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayer : MonoBehaviour
{
    public GameObject Target; // положение персонажа
    public IndicatorPlayer TargetIndicator; // Индикатор

    public float dumping = 2f; // сглаживание камеры. скольжение
    public float distance = 2f;

    public bool IsLimit = false;
    public bool DebugCamera = false;

    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!Target)
        {
            if (PhysicalEntity.EntityLists.playerEnts != null && PhysicalEntity.EntityLists.playerEnts.Count > 0)
                for (int i = 0; i < PhysicalEntity.EntityLists.playerEnts.Count; i++)
                    if (PhysicalEntity.EntityLists.playerEnts[i] != null && PhysicalEntity.EntityLists.playerEnts[i].PlayerProperties.isLocalPlayer)
                        Target = PhysicalEntity.EntityLists.playerEnts[i].Obj;
        }

        if (Target)
        {
            if (!TargetIndicator)
                TargetIndicator = Target.transform.Find("PointTarget").GetComponent<IndicatorPlayer>();

            Vector3 target = Target.transform.position + (Target.transform.up * distance);

            Vector3 currentPosition = Vector3.Lerp(transform.position, new Vector3(target.x, target.y, transform.position.z), dumping * Time.deltaTime);
            transform.position = currentPosition;
        }

        if (IsLimit)
        {
            float leftBound = transform.position.x - Camera.main.orthographicSize / Screen.height * Screen.width;
            float rightBound = transform.position.x + Camera.main.orthographicSize / Screen.height * Screen.width;
            float upBound = transform.position.y + Camera.main.orthographicSize;
            float downBound = transform.position.y - Camera.main.orthographicSize;

            // Debug.Log($"leftBound: {leftBound} | rightBound: {rightBound} | upBound {upBound} | downBound: {downBound}");

            transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, LimitCamera.limitCamera.leftWall.position.x - (leftBound - transform.position.x), LimitCamera.limitCamera.rightWall.position.x - (rightBound - transform.position.x)),
            Mathf.Clamp(transform.position.y, LimitCamera.limitCamera.downWall.position.y - (downBound - transform.position.y), LimitCamera.limitCamera.upWall.position.y - (upBound - transform.position.y)),
            transform.position.z);
        }
    }

    public void ChangePlayerTarget(GameObject CurrentTarget)
    {
        if (Target == CurrentTarget)
            return;

        TargetIndicator.ChangeIndicator(IndicatorPlayer.Indicator.none);

        Target = CurrentTarget;
        TargetIndicator = Target.transform.Find("PointTarget").GetComponent<IndicatorPlayer>();

        TargetIndicator.ChangeIndicator(IndicatorPlayer.Indicator.LocalPlayer);
    }

    private void OnDrawGizmos()
    {
        if (!DebugCamera)
            return;

        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector2(LimitCamera.limitCamera.leftWall.position.x, LimitCamera.limitCamera.upWall.position.y), new Vector2(LimitCamera.limitCamera.rightWall.position.x, LimitCamera.limitCamera.upWall.position.y));
        Gizmos.DrawLine(new Vector2(LimitCamera.limitCamera.leftWall.position.x, LimitCamera.limitCamera.upWall.position.y), new Vector2(LimitCamera.limitCamera.leftWall.position.x, LimitCamera.limitCamera.downWall.position.y));
        Gizmos.DrawLine(new Vector2(LimitCamera.limitCamera.leftWall.position.x, LimitCamera.limitCamera.downWall.position.y), new Vector2(LimitCamera.limitCamera.rightWall.position.x, LimitCamera.limitCamera.downWall.position.y));
        Gizmos.DrawLine(new Vector2(LimitCamera.limitCamera.rightWall.position.x, LimitCamera.limitCamera.upWall.position.y), new Vector2(LimitCamera.limitCamera.rightWall.position.x, LimitCamera.limitCamera.downWall.position.y));
    }
}
