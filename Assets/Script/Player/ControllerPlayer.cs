﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerPlayer : NetworkBehaviour
{
    [SyncVar] public Vector2 TargetMove;
    public Player TargetAim;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Player _player;

    private Coroutine CorPlayerDanger;

    [SerializeField] private GameObject FirePoint;

    [SerializeField] private float MinTimerDelayDetect = 0.5f;
    [SerializeField] private float MaxTimerDelayDetect = 1.0f;
    [SerializeField] private float RadiusPlayerDetect = 2.5f;

    [SerializeField] private float LookSpeedNormal = 5f;
    [SerializeField] private float LookSpeedDanger = 10f;

    void Start()
    {
        CorPlayerDanger = null;
    }

    void OnEnable()
    {
        GameObjStateControl.EventSpawnPlayer_After += Spawn;
        Biohazard.EventInfectionPlayer += InfectionPlayer;
    }

    void OnDisable()
    {
        GameObjStateControl.EventSpawnPlayer_After -= Spawn;
        Biohazard.EventInfectionPlayer -= InfectionPlayer;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (isServer)
            PlayerSpeed();

        if (_player.IsAlive)
        {
            TargetMove = TargetMove.normalized * _player.speed * Time.deltaTime;

            AnimationObj.AnimationPlayerFeet(_player.Feet,
                GameControl.GetIsFreeze ? 0f : (TargetMove.magnitude / Time.deltaTime) / 2f
                , TargetMove);

            if (TargetMove != Vector2.zero)
            {
                if (!GameControl.GetIsFreeze)
                {
                    if(rb.velocity == Vector2.zero)
                        rb.MovePosition(rb.position + TargetMove);
                }

                if (!_player.IsDanger)
                {
                    Look(TargetMove, LookSpeedNormal);
                }
            }

            if (!isServerOnly)
            {
                if(CorPlayerDanger == null)
                    CorPlayerDanger = StartCoroutine(PlayerDanger());

                if (TargetAim && TargetAim.IsAlive)
                {
                    Look(TargetAim.transform.position - transform.position, LookSpeedDanger);
                }
                else
                    TargetAim = null;
            }
        }
    }

    void PlayerSpeed()
    {
        if (rb.velocity == Vector2.zero)
        {
            switch (_player.PlayerTeam)
            {
                case Player.Team.Zombie: { _player.speed = _player.ZombieClass.Speed * (_player.IsPainShock ? _player.ZombieClass.PainShock : 1f); break; }
                case Player.Team.Human: { _player.speed = _player.HumanClass.Speed * (_player.IsPainShock ? _player.HumanClass.PainShock : 1f); break; }
            }
        }
        else
            _player.speed = 0f;
    }

    public void Look(Vector2 Target, float speed)
    {
        float angle = Mathf.Atan2(Target.x, Target.y) * Mathf.Rad2Deg;
        angle = -angle;

        //transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, 0, angle)), speed * Time.deltaTime);
    }

    public IEnumerator PlayerDanger()
    {
        if(TargetAim)
            yield return new WaitForSeconds(Random.Range(MinTimerDelayDetect, MaxTimerDelayDetect));
        else
            yield return new WaitForSeconds(0.05f);

        Player TempTargetEnemy = TargetSearch.PlayerSearch(_player, RadiusPlayerDetect);

        if (TempTargetEnemy)
        {
            _player.IsDanger = true;
            TargetAim = TempTargetEnemy;
        }
        else
        {
            _player.IsDanger = false;
            TargetAim = null;
        }

        CorPlayerDanger = null;
    }

    public void ChangeTargetMove(Vector2 Move)
    {
        if (Move == TargetMove)
            return;

        if (isServer)
        {
            //RpcChangeTargetMove(Move);
            TargetMove = Move;
        }
        else
        {
            CmdChangeTargetMove(Move);
            TargetMove = Move;
        }
    }

    public void Spawn(Entity.PlayerEnt Player, StatusFunc status)
    {
        if (Player.id != _player.GetPlayerID)
            return;

        if (CorPlayerDanger != null)
        {
            StopCoroutine(CorPlayerDanger);
            CorPlayerDanger = null;
        }
    }

    void InfectionPlayer(Player player, int id)
    {
        if (id != _player.GetPlayerID)
            return;

        if (CorPlayerDanger != null)
        {
            StopCoroutine(CorPlayerDanger);
            CorPlayerDanger = null;
        }
    }

    //[ClientRpc]
    //void RpcChangeTargetMove(Vector2 Move)
    //{
    //    if (isServer)
    //        return;

    //    TargetMove = Move;
    //}

    [Command]
    void CmdChangeTargetMove(Vector2 Move)
    {
        if (!isServer)
            return;

        TargetMove = Move;
    }
}
