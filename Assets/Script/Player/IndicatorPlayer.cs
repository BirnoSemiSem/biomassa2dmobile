﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorPlayer : MonoBehaviour
{
    public enum Indicator
    {
        LocalPlayer,
        Player,
        Bot,
        none
    }

    [SerializeField] private readonly int ConstMainLayer = 100;

    public SpriteRenderer PointSprTarget; // Point Target персонажа
    public SpriteRenderer PlayerSprTarget; // Point Local Player персонажа
    public SpriteRenderer FeetSprTarget; // Point Local Player персонажа
    public SpriteRenderer WeaponSprTarget; // Point Local Player персонажа
    public Player PlayerProperties; // положение персонажа

    [SerializeField] private Sprite LocalPlayerInd; // Индиактор локального игрока
    [SerializeField] private Sprite PlayerInd; // Индиактор игрока
    [SerializeField] private Sprite BotInd; // Индиактор Бота

    private bool IsVisible;
    [SerializeField] private bool IsOptimization;

    // Update is called once per frame
    void Update()
    {
        if (IsVisible)
        {
            if (PlayerProperties.IsAlive && PointSprTarget.sprite == null)
            {
                if (PlayerProperties.isLocalPlayer)
                {
                    PointSprTarget.sprite = LocalPlayerInd;

                    ChangeLayerPlayer(ConstMainLayer);
                }
                else if (!PlayerProperties.Bot)
                {
                    PointSprTarget.sprite = PlayerInd;

                    ChangeLayerPlayer(-ConstMainLayer);
                }
                else
                {
                    PointSprTarget.sprite = BotInd;

                    ChangeLayerPlayer(-ConstMainLayer);
                }
            }
            else if (!PlayerProperties.IsAlive && PointSprTarget.sprite != null)
                PointSprTarget.sprite = null;
        }
    }

    public void ChangeIndicator(Indicator Target)
    {
        switch(Target)
        {
            case Indicator.LocalPlayer:
            {
                PointSprTarget.sprite = LocalPlayerInd;

                ChangeLayerPlayer(ConstMainLayer);
                break;
            }
            case Indicator.Player:
            {
                PointSprTarget.sprite = PlayerInd;

                ChangeLayerPlayer(-ConstMainLayer);
                break;
            }
            case Indicator.Bot:
            {
                PointSprTarget.sprite = BotInd;

                ChangeLayerPlayer(-ConstMainLayer);
                break;
            }
            case Indicator.none:
            {
                PointSprTarget.sprite = null;

                ChangeLayerPlayer(-ConstMainLayer);
                break;
            }
        }
    }

    void ChangeLayerPlayer(int AddLayer)
    {
        if(AddLayer < 0)
        {
            if(PointSprTarget.sortingOrder > ConstMainLayer)
            {
                PointSprTarget.sortingOrder -= ConstMainLayer;
                PlayerSprTarget.sortingOrder -= ConstMainLayer;
                FeetSprTarget.sortingOrder -= ConstMainLayer;
                WeaponSprTarget.sortingOrder -= ConstMainLayer;
            }
        }
        else if(AddLayer > 0)
        {
            if(PointSprTarget.sortingOrder < ConstMainLayer)
            {
                PointSprTarget.sortingOrder += ConstMainLayer;
                PlayerSprTarget.sortingOrder += ConstMainLayer;
                FeetSprTarget.sortingOrder += ConstMainLayer;
                WeaponSprTarget.sortingOrder += ConstMainLayer;
            }
        }
    }

    void OnBecameVisible()
    {
        if (gameObject && IsOptimization)
        {
            PointSprTarget.color = new Color(255f, 255f, 255f, 255f);
            IsVisible = true;
        }
    }

    void OnBecameInvisible()
    {
        if (gameObject && IsOptimization)
        {
            PointSprTarget.color = new Color(255f, 255f, 255f, 0f);
            IsVisible = false;
        }
    }
}
